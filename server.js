
const express = require('express');
const path = require('path');
const app = express();

app.use(express.static(__dirname + '/dist/steam'));
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/src/app/app.component.html'));
});
app.listen(process.env.PORT || 4200);

// const express = require('express');
// const path = require('path');

// const app = express();

// // Serve only the static files form the dist directory
// app.use(express.static('./dist/SteamApp'));

// app.get('/*', (req, res) =>
//     res.sendFile('index.html', {root: 'dist/SteamApp/'}),
// );

// // Start the app by listening on the default Heroku port
// app.listen(process.env.PORT || 8080);