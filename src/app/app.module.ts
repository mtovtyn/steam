import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FirebaseTSApp} from 'firebasets/firebasetsApp/firebaseTSApp';
import { environment } from 'src/environments/environment';
import { MatButtonModule } from '@angular/material/button';
import { HomeComponent } from './pages/home/home.component';
import{ MatCardModule } from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ProfileComponent } from './pages/profile/profile.component';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { NavigationBarComponent } from './pages/navigation-bar/navigation-bar.component';
import { FriendsComponent } from './pages/friends/friends.component';
import { UserComponent } from './tools/user/user.component';
import { GamesComponent } from './pages/games/games.component';
import { GameComponent } from './tools/game/game.component';
import {MatSliderModule} from '@angular/material/slider';
import { LibraryComponent } from './pages/library/library.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    NavigationBarComponent,
    FriendsComponent,
    UserComponent,
    GamesComponent,
    GameComponent,
    LibraryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    FormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatSliderModule
  ],
  providers: [GameComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(){
    FirebaseTSApp.init(environment.firebaseConfig);
  }
 }

 
