import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FriendsComponent } from './pages/friends/friends.component';
import { GamesComponent } from './pages/games/games.component';
import { HomeComponent } from './pages/home/home.component';
import { LibraryComponent } from './pages/library/library.component';
import { NavigationBarComponent } from './pages/navigation-bar/navigation-bar.component';
import { ProfileComponent } from './pages/profile/profile.component';
const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "profile/:id", component: ProfileComponent},
  {path: "profile", component: ProfileComponent},
  {path: "navigation", component: NavigationBarComponent},
  {path: "friends/:id", component: FriendsComponent},
  {path: "games/:id", component: GamesComponent},
  {path: "library/:id", component: LibraryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
