import { Component, OnInit } from '@angular/core';
import * as firebase from "firebase/app";
import 'firebase/firestore';
//import { arr } from '../home/home.component';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';
import { FirebaseTSFirestore, Limit, OrderBy, Where } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';
import * as e from 'express';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  myFriends: string[] = [];
  firestore: FirebaseTSFirestore;
  user: any;

  auth: FirebaseTSAuth;
  public emaill: string | null;
  email = history.state.data;

  constructor(private _Activatedroute: ActivatedRoute) {

    this.firestore = new FirebaseTSFirestore();
    this.auth = new FirebaseTSAuth();
    this.emaill = this._Activatedroute.snapshot.paramMap.get("id");
    this.auth.checkSignInState(
      {
        whenSignedIn: user => {

          this.emaill = user.email;
        }


      })
  }


  ngOnInit(): void {

    console.log(this.emaill);
  }
  isNotEmpty(text: string) {
    return text != null && text.length > 0;


  }

  onContinueClick(
    username: HTMLInputElement,
    email: HTMLInputElement,
    age: HTMLInputElement
  ) {
    let username1 = username.value;
    let email1 = email.value;
    let age1 = age.value;
    if (this.isNotEmpty(username1) &&
      this.isNotEmpty(age1)) {
      this.firestore.create({
        path: ["Users", this.auth.getAuth().currentUser!.uid],
        data: {
          publicName: username1,
          email: email1,
          age: age1
        },
        onComplete: async (docId) => {
          alert("Profile created");
          console.log(this.firestore.getCollection({
            path: ["Users"],
            where: [new OrderBy("age", "desc"),
            new Limit(10)]
          }));

        },
        onFail: (err) => {

        }
      })
    }
    else {
      alert('No empty fields please')
    }
  }


}

