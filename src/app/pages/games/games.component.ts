import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
  what=false;
   finalistsCollection = [
    {rev:0, checked: false, name: "Little Big Snake.io", description:'Little Big Snake is the ultimate slither style game. You may be big but itll take more than that to survive in the dewy world of Ambrosia! Little Big Snake is a slippery game of serpentine fun. On the jungle floor, deep beneath its leafy canopy, you are forced to defend yourself from a wide variety of potential threats using only your mouse and some hard-earned upgrades.', genre: "Action", price: 200 },
    {rev:0, checked: false, name: "No-El", description: 'No-El is a free escape game. Oh no its time for another holiday and its up to you to save that holiday. If you arent able to finish your holiday-themed mission then there will not be a holiday for all of the good boys and girls of the world who have earned gifts through their behavior and general affect for the previous year.', genre: "Adventure", price: 300  },
    {rev:0, checked: false, name: "Inklink", description: 'Inklink.io is a free drawing game. Have you ever wanted to draw stick figures competitively with friends or enemies from around the globe? Well, Inklink.io knows what you dream and has delivered you exactly what you want! Inklink.io is a game where you can draw with your friends.' , genre: "Indie", price: 400  },
    {rev:0, checked: false, name: "Mope", description: 'Mope.io is a free arena, survival game. Welcome to the jungle where evolution rules all. Choose your animal and jump into the arena. Will you be a shrimp, a mouse, a gentle bird, or a squirrel? The choice is yours and once you make it youll be forced to survive in a world gone mad.', genre: "Adventure", price: 200  },
    {rev:0, checked: false, name: "Balls And Bricks", description: 'Balls And Bricks is a free shooter game. Take aim and blast away in this exciting, fast-paced shooter game where youll be tasked with the goal of blasting your way through as many bricks with as few balls as possible. Use geometry to your advantage and bank your shots at exactly the right angle to maximize your total output. ', genre: "Action", price: 400  },
    {rev:0, checked: false, name: "Forge of Empires", description: 'Forge of Empires is a truly epic MMO game. Do you think Empires are negotiated over biscuits and tea? Fool! The world around you was forged in the flames of time and against the cold iron of experience. All empires started as an idea which was brought forth into the world and made real by hard work, determination, and strategies of their founders. ', genre: "Indie", price: 300  }
  ];
  filteredCollection : finalistsCollection1 [] = [];
  libraryCollection : finalistsCollection1 [] = [];
  constructor(private router: Router){

  }
  get SumVaalue(){
    return this.libraryCollection;
  }
  searchUsers(search :HTMLInputElement){
    if (search) {
      this.filteredCollection = this.finalistsCollection.filter(
        x => x.name == search.value
      );
    
    } else {
      this.filteredCollection = this.finalistsCollection;
    }

  }
  addToLibrary(name: string){
    for(let i=0; i<this.finalistsCollection.length;i++){
      if(this.finalistsCollection[i].name==name){
        this.finalistsCollection[i].rev+=1;
        if(this.finalistsCollection[i].rev<=1){
       this.libraryCollection.push(this.finalistsCollection[i]);
        } else{
          for(let i = 0; i < this.libraryCollection.length; i++){ 
    
            if ( this.libraryCollection[i].rev>1) { 
        
                this.libraryCollection.splice(i, 1); 
            }
        }
      }
       console.log(this.libraryCollection.length);
        } 
    }
   // this.router.navigate(['secondcomponenturl'], {queryParams: {data : this.libraryCollection}});
  }

  onSliderChange() {
    for(let i=0; i<this.finalistsCollection.length;i++){

          this.finalistsCollection[i].checked=false;

    }

   let a=Number((<HTMLInputElement>document.getElementById("slider")).value);
      //   this.filteredCollection = this.finalistsCollection.filter(
      //     x => x.genre == genre
      //   );
      // 
        for(let i=0; i<this.finalistsCollection.length;i++){
          if(this.finalistsCollection[i].price==a){
            if( this.finalistsCollection[i].checked){
              this.finalistsCollection[i].checked=false;
            } else {
              this.finalistsCollection[i].checked=true;
            }
         
          }
        }
        this.fill(this.finalistsCollection);
      
    
    }

  filterData(genre: string = "") {
    if (genre) {
    //   this.filteredCollection = this.finalistsCollection.filter(
    //     x => x.genre == genre
    //   );
    // 
      for(let i=0; i<this.finalistsCollection.length;i++){
        if(this.finalistsCollection[i].genre==genre){
          if( this.finalistsCollection[i].checked){
            this.finalistsCollection[i].checked=false;
          } else {
            this.finalistsCollection[i].checked=true;
          }
       
        }
      }
      this.fill(this.finalistsCollection);
    } else {
      this.filteredCollection = this.finalistsCollection;
    }
  }

  fill(lala: finalistsCollection1[]){
    this.filteredCollection = this.finalistsCollection.filter(
         x => x.checked
        );
  }

  ngOnInit(){
    this.filterData();
  }
}

export interface finalistsCollection1{
  rev: number,
  checked: boolean,
  name: string,
  description: string,
  genre: string,
  price: number;
}

