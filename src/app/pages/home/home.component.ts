import { Component, OnInit } from '@angular/core';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {


state = AuthenticatorCompState.LOGIN;
firebasetsAuth: FirebaseTSAuth;

  constructor(private router: Router) {
    this.firebasetsAuth=new FirebaseTSAuth();

   }
   gotoProfile(text: string){
    this.router.navigate(['navigation/'+text]); 
    this.router.navigate(['profile/'+text]); 
    return text;
   }

  ngOnInit(): void {
 
  }
  


  onLoginClick(loginEmail: HTMLInputElement,
    loginPassword: HTMLInputElement){

     
      let email = loginEmail.value;
      let password = loginPassword.value;
      if(this.isNotEmpty(email)&&
      this.isNotEmpty(password)){
        this.firebasetsAuth.createAccountWith(
          {
            
            email: email,
            password: password,

            onComplete: (uc) => {
              console.log(email);
             this.gotoProfile(email);
          // alert("Acco");
          // loginEmail.value="";
          // loginPassword.value="";
            },
            onFail: (err) => {
              alert("Error!");
            }
          }
        );
      }
     
      
this.state=AuthenticatorCompState.LOGIN;
  }

  isNotEmpty(text: string){
    return text!=null && text.length>0;
  }
isLoginState(){
  return this.state==AuthenticatorCompState.LOGIN;
}


}

export enum AuthenticatorCompState {
  LOGIN
}

