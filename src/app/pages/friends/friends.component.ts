import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';
import { FirebaseTSFirestore, OrderBy, Where } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

  firestore: FirebaseTSFirestore;
  users: UserData[] = [];

  friends: UserData[] = [];
  constructor(private _Activatedroute: ActivatedRoute) {
    this.firestore = new FirebaseTSFirestore();
  }

  ngOnInit(): void {
  
  }
  searchUsers(search: HTMLInputElement) {

    this.users = [];
    this.firestore.getCollection(
      {
        path: ["Users"],
        where: [
          new Where('publicName', '==', search.value)
        ],
        onComplete: (result) => {

          result.docs.forEach(
            doc => {
              let user = <UserData>doc.data();
              this.users.push(user);
            }
          );
        },
        onFail: err => {

        }
      }
    );

  }

}

export interface UserData {
  publicName: string;
  email: string;
  age: number;
  friends: []
}
