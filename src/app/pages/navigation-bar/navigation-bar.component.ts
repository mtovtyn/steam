import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {
public l:string|null;
  constructor(private _Activatedroute:ActivatedRoute) { 
 
    this.l=this._Activatedroute.snapshot.paramMap.get("id");
  }

  ngOnInit(): void {
  }

}
