import { Component, Injectable, Input, OnInit } from '@angular/core';

import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';
import { FirebaseTSFirestore, Where } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';
import { FriendsComponent, UserData } from 'src/app/pages/friends/friends.component';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  myFriends1: string[] = [];
  toggle: boolean = true;
  text: string = 'Add Friend';
  // status = 'Enable'; 
  users: UserData[] = [];
  firestore: FirebaseTSFirestore;
  auth: FirebaseTSAuth;
  // lala: UserData[]=[];
  @Input() userData: UserData | undefined;
  constructor() {
    this.firestore = new FirebaseTSFirestore();
    this.auth = new FirebaseTSAuth();
  }

  ngOnInit(): void {

  }
  changeColor(la: string) {
    this.toggle = !this.toggle;
    if (this.text === 'Add Friend') {
      this.myFriends1.push(la);
      console.log(this.myFriends1);
      this.text = 'Delete Friend'
    } else {
      this.text = 'Add Friend'
      const index = this.myFriends1.indexOf(la);

      if (index !== -1) {
        this.myFriends1.splice(index, 1);
      }
    }
  }

  addFriend(user: UserData | undefined) {

    this.firestore.update({

      path: ["Users", this.auth.getAuth().currentUser!.uid],
      data: {
        friends: [user]
      },
      onComplete: async (docId) => {
        this.changeColor(user!.publicName);
      },
      onFail: (err) => {

      }
    })
  }

}


