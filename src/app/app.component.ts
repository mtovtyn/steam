import { Component } from '@angular/core';
import { MatButton } from '@angular/material/button';
;
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SteamApp';
  auth = new FirebaseTSAuth();

  constructor() {
    this.auth.listenToSignInStateChanges(
      user => {
        this.auth.checkSignInState(
          {
            whenSignedIn: user => {

            },
            whenSignedOut: user => {
            }
          }
        )
      }
    );
  }
  loggedIn() {
    return this.auth.isSignedIn();
  }
  onLogoutClick() {
    this.auth.signOut();
  }
}
