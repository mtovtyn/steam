// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { getFirestore } from "firebase/firestore";

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAIGywy0ijhE7Qe09CDsGsq_pdJGI-rykA",
    authDomain: "steamapp-581f5.firebaseapp.com",
    projectId: "steamapp-581f5",
    storageBucket: "steamapp-581f5.appspot.com",
    messagingSenderId: "275558114957",
    appId: "1:275558114957:web:0fabdffdbb83fdc0a264c4",
    measurementId: "G-RH3HNT0N7B"
  }
  
};


// For Firebase JS SDK v7.20.0 and later, measurementId is optional

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
